package com.example.sync_android_room.dao

import androidx.room.*
import com.example.sync_android_room.model.Person

@Dao
interface PersonDao {

    @Insert
    suspend fun insertPerson(person: Person)

    @Update
    suspend fun updatePerson(person: Person)

    @Delete
    suspend fun deletePerson(person: Person)

    @Query("DELETE FROM Persons")
    suspend fun deleteAllPersons()

    @Query("SELECT * FROM Persons WHERE id = :id")
    suspend fun getPerson(id: Int): Person

    @Query("SELECT * FROM Persons ORDER BY name ASC")
    suspend fun getPersons(): List<Person>

}