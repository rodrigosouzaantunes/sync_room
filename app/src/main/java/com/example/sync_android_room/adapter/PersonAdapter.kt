package com.example.sync_android_room.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sync_android_room.databinding.PersonItemBinding
import com.example.sync_android_room.model.Person

class PersonAdapter(
    private val onDeleteClick: (Person) -> Unit,
    private val onCardClick: (Int)-> Unit
) :ListAdapter<Person, PersonAdapter.PersonViewHolder>(DiffCallBack()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val binding = PersonItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return PersonViewHolder(
            binding = binding,
            onCardClick = onCardClick,
            onDeleteClick = onDeleteClick
        )
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class PersonViewHolder(
        private val binding: PersonItemBinding,
        private val onCardClick: (Int) -> Unit,
        private val onDeleteClick: (Person) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(person: Person) = binding.apply {
            textViewName.text = person.name
            textViewAge.text = person.age.toString()
            textViewPhone.text = person.phoneNumber
            textViewAddress.text = person.address
            cardPerson.setOnClickListener {
                onCardClick(person.id)
            }
            buttonDelete.setOnClickListener {
                onDeleteClick(person)
            }
        }
    }
}

class DiffCallBack : DiffUtil.ItemCallback<Person>(){
    override fun areItemsTheSame(oldItem: Person, newItem: Person) = oldItem == newItem
    override fun areContentsTheSame(oldItem: Person, newItem: Person) = oldItem.id == newItem.id
}