package com.example.sync_android_room

import com.example.sync_android_room.database.PersonDatabase
import com.example.sync_android_room.viewmodel.MainActivityViewModel
import com.example.sync_android_room.viewmodel.UpdateActivityViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val koinModule = module {

    single {
        PersonDatabase.getDatabase(androidContext())
    }

    viewModel{
        MainActivityViewModel(get())
    }

    viewModel{
        UpdateActivityViewModel(get())
    }

}