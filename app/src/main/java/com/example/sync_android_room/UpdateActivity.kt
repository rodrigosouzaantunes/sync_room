package com.example.sync_android_room

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sync_android_room.databinding.ActivityUpdateBinding
import com.example.sync_android_room.model.Person
import com.example.sync_android_room.viewmodel.UpdateActivityViewModel
import org.koin.android.ext.android.inject

class UpdateActivity : AppCompatActivity() {

    private val viewModel: UpdateActivityViewModel by inject()
    private val binding: ActivityUpdateBinding by lazy {
        ActivityUpdateBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val idPerson = intent.getIntExtra("id",0)
        setObservers()
        supportActionBar?.title = "Cadastro"
        configScreen(idPerson)
        setListeners()
        setContentView(binding.root)
    }

    private fun configScreen(idPerson: Int) = binding.apply {
        if (idPerson != 0){
            updateConfig(idPerson)
        }else{
            insertConfig()
        }

    }

    private fun updateConfig(idPerson: Int) {
        viewModel.getPerson(idPerson)
        binding.apply {
            buttonSave.setOnClickListener {
                val person = viewModel.person.value!!

                person.apply {
                    name = editTextName.text.toString()
                    age = editTextAge.text.toString().toInt()
                    phoneNumber = editTextPhoneNumber.text.toString()
                    address = editTextAddress.text.toString()
                }
                 viewModel.updatePerson(person)
                onBackPressed()
            }
        }
    }

    private fun insertConfig() {
        binding.apply {
            buttonSave.setOnClickListener {
                viewModel.insertPerson(
                    Person(
                        name = editTextName.text.toString(),
                        age = editTextAge.text.toString().toInt(),
                        phoneNumber = editTextPhoneNumber.text.toString(),
                        address = editTextAddress.text.toString()
                    )
                )
                onBackPressed()
            }
        }
    }

    private fun setListeners() = binding.apply {

    }

    private fun setObservers() {
        viewModel.person.observe(this,{ person ->
            binding.apply {
                editTextName.setText(person.name)
                editTextAge.setText(person.age.toString())
                editTextPhoneNumber.setText(person.phoneNumber)
                editTextAddress.setText(person.address)
            }
        })
    }
}