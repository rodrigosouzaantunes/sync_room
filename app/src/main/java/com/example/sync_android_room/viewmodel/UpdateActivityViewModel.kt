package com.example.sync_android_room.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sync_android_room.database.PersonDatabase
import com.example.sync_android_room.model.Person
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UpdateActivityViewModel(private val database: PersonDatabase) : ViewModel() {

    private val _person = MutableLiveData<Person>()
    val person : LiveData<Person> = _person

    fun getPerson(id: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _person.postValue(database.personDao().getPerson(id))
        }
    }

    fun insertPerson(person: Person){
        viewModelScope.launch(Dispatchers.IO) {
            database.personDao().insertPerson(person)
        }
    }

    fun updatePerson(person: Person){
        viewModelScope.launch(Dispatchers.IO) {
            database.personDao().updatePerson(person)
        }
    }

}