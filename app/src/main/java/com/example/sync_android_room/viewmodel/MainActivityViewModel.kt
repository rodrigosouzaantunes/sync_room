package com.example.sync_android_room.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sync_android_room.database.PersonDatabase
import com.example.sync_android_room.model.Person
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivityViewModel( private val database: PersonDatabase ): ViewModel() {

 private val _persons = MutableLiveData<List<Person>>()
 val persons: LiveData<List<Person>> = _persons

    fun getPersons(){
        viewModelScope.launch(Dispatchers.IO) {
            _persons.postValue(database.personDao().getPersons())
        }
    }

    fun deletePerson(person: Person){
        viewModelScope.launch(Dispatchers.IO) {
            database.personDao().deletePerson(person)
            getPersons()
        }
    }

    fun deleteAllPersons(){
        viewModelScope.launch(Dispatchers.IO) {
            database.personDao().deleteAllPersons()
            getPersons()
        }
    }
}