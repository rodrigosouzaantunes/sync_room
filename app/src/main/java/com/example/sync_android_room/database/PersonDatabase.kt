package com.example.sync_android_room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.sync_android_room.dao.PersonDao
import com.example.sync_android_room.model.Person

@Database(
    entities = [Person::class],
    version = 1,
    exportSchema = false
)
abstract class PersonDatabase: RoomDatabase() {

    abstract fun personDao(): PersonDao

    companion object{
        private const val DATABASE_NAME: String = "Person_Database"

        fun getDatabase(context: Context): PersonDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                PersonDatabase::class.java,
                DATABASE_NAME
            ).build()
        }
    }
}