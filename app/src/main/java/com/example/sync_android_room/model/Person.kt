package com.example.sync_android_room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Persons")
data class Person (
    var name: String,
    var age: Int,
    @ColumnInfo(name = "phone_number")
    var phoneNumber: String,
    var address: String
        ){
    @PrimaryKey(autoGenerate = true)
    var id = 0
}