package com.example.sync_android_room

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sync_android_room.adapter.PersonAdapter
import com.example.sync_android_room.databinding.ActivityMainBinding
import com.example.sync_android_room.model.Person
import com.example.sync_android_room.viewmodel.MainActivityViewModel
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by inject()
    private lateinit var personAdapter: PersonAdapter

    private val binding : ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setButtonClick()
        setRecycler()
        setObservers()
        supportActionBar?.title = "Pessoas"
        viewModel.getPersons()
        setContentView(binding.root)
    }

    override fun onResume() {
        viewModel.getPersons()
        super.onResume()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.menu_delete_all ->{
                viewModel.deleteAllPersons()
            }
        }
        return true
    }

    private fun setRecycler() {
        personAdapter = PersonAdapter(
            onDeleteClick = onDeleteButtonClick(),
            onCardClick = onCardClick()
        )
        binding.recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            this.adapter = personAdapter
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setObservers() {
        viewModel.persons.observe(this, { list->
            personAdapter.submitList(list)
        })
    }

    private fun setButtonClick() = binding.floatingActionButton.setOnClickListener {
        val intent = Intent(this, UpdateActivity::class.java)
        startActivity(intent)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun onDeleteButtonClick(): (Person) -> Unit {
        return {person ->
            viewModel.deletePerson(person)
        }
    }

    private fun onCardClick(): (Int) -> Unit {
        return {id ->
            var intent = Intent(this, UpdateActivity::class.java).putExtra("id",id)
            startActivity(intent)
        }
    }
}